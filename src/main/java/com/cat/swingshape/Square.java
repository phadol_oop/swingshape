/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cat.swingshape;

/**
 *
 * @author Black Dragon
 */
public class Square extends Rectangle {
    
    private double side;
    
    public Square(double side) {
        super(side);
        this.side = side;
    }

    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        this.side = side;
    }
}
