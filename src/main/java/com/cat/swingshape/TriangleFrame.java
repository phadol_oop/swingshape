/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cat.swingshape;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Black Dragon
 */
public class TriangleFrame extends JFrame {
    JLabel lblBase;
    JTextField txtBase;
    JLabel lblHeight;
    JTextField txtHeight;
    JLabel lblSideA;
    JTextField txtSideA;
    JLabel lblSideB;
    JTextField txtSideB;
    JButton btnCalculate;
    JLabel lblResult;
    
    public TriangleFrame() {
        super("Triangle");
        this.setSize(355, 355);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);
        
        lblBase = new JLabel("Base:", JLabel.TRAILING);
        lblBase.setSize(35, 20);
        lblBase.setLocation(5, 5);
        lblBase.setBackground(Color.WHITE);
        lblBase.setOpaque(true);
        this.add(lblBase);
        
        txtBase = new JTextField();
        txtBase.setSize(35, 20);
        txtBase.setLocation(45, 5);
        this.add(txtBase);
        
        lblHeight = new JLabel("Height:", JLabel.TRAILING);
        lblHeight.setSize(40, 20);
        lblHeight.setLocation(85, 5);
        lblHeight.setBackground(Color.WHITE);
        lblHeight.setOpaque(true);
        this.add(lblHeight);
        
        txtHeight = new JTextField();
        txtHeight.setSize(35, 20);
        txtHeight.setLocation(130, 5);
        this.add(txtHeight);
        
        lblSideA = new JLabel("SideA:", JLabel.TRAILING);
        lblSideA.setSize(40, 20);
        lblSideA.setLocation(170, 5);
        lblSideA.setBackground(Color.WHITE);
        lblSideA.setOpaque(true);
        this.add(lblSideA);
        
        txtSideA = new JTextField();
        txtSideA.setSize(35, 20);
        txtSideA.setLocation(215, 5);
        this.add(txtSideA);
        
        lblSideB = new JLabel("SideB:", JLabel.TRAILING);
        lblSideB.setSize(40, 20);
        lblSideB.setLocation(255, 5);
        lblSideB.setBackground(Color.WHITE);
        lblSideB.setOpaque(true);
        this.add(lblSideB);
        
        txtSideB = new JTextField();
        txtSideB.setSize(35, 20);
        txtSideB.setLocation(300, 5);
        this.add(txtSideB);
        
        btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 40);
        this.add(btnCalculate);
        
        lblResult = new JLabel("<html><center>Triangle base = ??? height = ??? sideA = ??? sideB = ??? <br> area = ??? perimeter = ???</center></html>");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(355, 50);
        lblResult.setLocation(0, 75);
        lblResult.setBackground(Color.cyan);
        lblResult.setOpaque(true);
        this.add(lblResult);
        
        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strBase = txtBase.getText();
                    double Base = Double.parseDouble(strBase);
                    String strHeight = txtHeight.getText();
                    double Height = Double.parseDouble(strHeight);
                    String strSideA = txtSideA.getText();
                    double SideA = Double.parseDouble(strSideA);
                    String strSideB = txtSideB.getText();
                    double SideB = Double.parseDouble(strSideB);

                    Triangle triangle = new Triangle(Base, Height, SideA, SideB);
                    lblResult.setText("<html><center>Triangle base = "+ String.format("%.2f", triangle.getBase())  
                            +" height = "+ String.format("%.2f", triangle.getHeight())  
                            +" sideA = "+ String.format("%.2f", triangle.getSideA())  
                            +" sideB = "+ String.format("%.2f", triangle.getSideB())  
                            +" <br> area = "+ String.format("%.2f", triangle.calArea())  
                            +" perimeter = "+ String.format("%.2f", triangle.calPerimeter())  +"</center></html>");

                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(TriangleFrame.this, "Error : Please input number."
                            , "Error", JOptionPane.ERROR_MESSAGE);
                    txtBase.setText("");
                    txtHeight.setText("");
                    txtSideA.setText("");
                    txtSideB.setText("");
                }
            }
        });
    }
    
    public static void main(String[] args) {
        TriangleFrame frame = new TriangleFrame();
        frame.setVisible(true);
    }
}