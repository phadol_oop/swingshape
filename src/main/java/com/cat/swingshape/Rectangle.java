/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cat.swingshape;

/**
 *
 * @author Black Dragon
 */
public class Rectangle extends Shape {

    private double sideA;
    private double sideB;
    
    public Rectangle(double sideA, double sideB) {
        super("Rectangle");
        this.sideA = sideA;
        this.sideB = sideB;
    }
    
    public Rectangle(double side) {
        super("Square");
        this.sideA = side;
        this.sideB = side;
    }

    public double getSideA() {
        return sideA;
    }

    public double getSideB() {
        return sideB;
    }

    public void setSideA(double sideA) {
        this.sideA = sideA;
    }

    public void setSideB(double sideB) {
        this.sideB = sideB;
    }

    @Override
    public double calArea() {
        return sideA * sideB;
    }

    @Override
    public double calPerimeter() {
        return 2 * sideA + 2 * sideB;
    }
    
}
