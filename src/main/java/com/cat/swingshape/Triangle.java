/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cat.swingshape;

/**
 *
 * @author Black Dragon
 */
public class Triangle extends Shape {

    private double base;
    private double height;
    private double sideA;
    private double sideB;
    
    public Triangle(double base, double height, double sideA, double sideB) {
        super("Triangle");
        this.base = base;
        this.height = height;
        this.sideA = sideA;
        this.sideB = sideB;
    }

    public double getBase() {
        return base;
    }

    public double getHeight() {
        return height;
    }

    public double getSideA() {
        return sideA;
    }

    public double getSideB() {
        return sideB;
    }

    public void setBase(double base) {
        this.base = base;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public void setSideA(double sideA) {
        this.sideA = sideA;
    }

    public void setSideB(double sideB) {
        this.sideB = sideB;
    }
    
    @Override
    public double calArea() {
        return 0.5 * base * height;
    }

    @Override
    public double calPerimeter() {
        return base + sideA + sideB;
    }
    
}
