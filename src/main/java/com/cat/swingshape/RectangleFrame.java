/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cat.swingshape;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Black Dragon
 */
public class RectangleFrame extends JFrame {

    JLabel lblSideA;
    JTextField txtSideA;
    JLabel lblSideB;
    JTextField txtSideB;
    JButton btnCalculate;
    JLabel lblResult;

    public RectangleFrame() {
        super("Rectangle");
        this.setSize(300, 300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);

        lblSideA = new JLabel("SideA:", JLabel.TRAILING);
        lblSideA.setSize(40, 20);
        lblSideA.setLocation(5, 15);
        lblSideA.setBackground(Color.WHITE);
        lblSideA.setOpaque(true);
        this.add(lblSideA);

        txtSideA = new JTextField();
        txtSideA.setSize(35, 20);
        txtSideA.setLocation(45, 15);
        this.add(txtSideA);

        lblSideB = new JLabel("SideB:", JLabel.TRAILING);
        lblSideB.setSize(40, 20);
        lblSideB.setLocation(85, 15);
        lblSideB.setBackground(Color.WHITE);
        lblSideB.setOpaque(true);
        this.add(lblSideB);

        txtSideB = new JTextField();
        txtSideB.setSize(35, 20);
        txtSideB.setLocation(130, 15);
        this.add(txtSideB);

        btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(175, 15);
        this.add(btnCalculate);

        lblResult = new JLabel("<html><center>Rectangle SideA = ??? SideB = ??? <br>area = ??? perimeter = ???</center></html>");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(300, 50);
        lblResult.setLocation(0, 75);
        lblResult.setBackground(Color.cyan);
        lblResult.setOpaque(true);
        this.add(lblResult);

        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strSideA = txtSideA.getText();
                    double SideA = Double.parseDouble(strSideA);
                    String strSideB = txtSideB.getText();
                    double SideB = Double.parseDouble(strSideB);

                    Rectangle rectangle = new Rectangle(SideA, SideB);
                    lblResult.setText("<html><center>Rectangle SideA = " + String.format("%.2f", rectangle.getSideA())
                            + " SideB = " + String.format("%.2f", rectangle.getSideB())
                            + " <br>area = " + String.format("%.2f", rectangle.calArea())
                            + " perimeter = " + String.format("%.2f", rectangle.calPerimeter())
                            + "</center></html>");

                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(RectangleFrame.this, "Error : Please input number.",
                             "Error", JOptionPane.ERROR_MESSAGE);
                    txtSideA.setText("");
                    txtSideB.setText("");
                }
            }
        });
    }

    public static void main(String[] args) {
        RectangleFrame frame = new RectangleFrame();
        frame.setVisible(true);
    }
}